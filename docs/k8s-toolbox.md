# Kubernetes toolbox 🧰

Here are some useful tools for Kubernetes:


## Local Cluster

If you want to run a local cluster and develop against or deploy to.

- [Minikube](https://minikube.sigs.k8s.io/)
- [MicroK8s](https://microk8s.io/)
- [k3d](https://k3d.io/)
- [kind](https://kind.sigs.k8s.io/)
- [Docker Desktop](https://www.docker.com/products/docker-desktop)

## Operate

If you want to operate, debug applications, and get an overview of the cluster.

- [Lens](https://lens.k8s.io/)
- [k9s](https://github.com/derailed/k9s)

## CLI

If you want to interact or ease some repetitive tasks.

- [kubectl](https://kubernetes.io/docs/reference/kubectl/overview/)
- [kubens](https://github.com/ahmetb/kubectx)
- [kubectx](https://github.com/ahmetb/kubectx)

## Development

- [okteto](https://github.com/okteto/okteto)
- [telepresence](https://www.telepresence.io/)
